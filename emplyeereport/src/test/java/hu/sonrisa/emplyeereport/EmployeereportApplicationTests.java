package hu.sonrisa.emplyeereport;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class EmployeereportApplicationTests {

	EmployeeReport reporter = new EmployeeReport();

	@Test
	void demoTest() {
		assertEquals(1, 1);
	}

	@Test
	void onlyOlderThanEightTeen() {

	}

	@Test
	void employeesWithUppercaseNames() {

	}

	@Test
	void employeesWithNameAscending() {

	}

	@Test
	void averageAge() {

	}


}
