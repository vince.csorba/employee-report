package hu.sonrisa.emplyeereport;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EmployeeReport {
    private List<Employee> dataSource = Arrays.asList(new Employee("Max", 17),
            new Employee("Sepp", 18),
            new Employee("Nina", 16),
            new Employee("Mike", 54),
            new Employee("Anna", 34),
            new Employee("Anna", 22));

    public List<Employee> GetEmployeesOlderThanEightTeen() {
        return null;
    }

    public List<Employee> GetEmployeesByNameAscending() {
        return null;

    }

    public List<Employee> GetEmployeesWithUppercaseNames() {
        return null;

    }

    public double GetAverageAge() {
        return 0;

    }
}
