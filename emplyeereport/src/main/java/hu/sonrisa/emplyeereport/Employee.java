package hu.sonrisa.emplyeereport;

public class Employee {

    public String nickName;
    public int age;

    public Employee(String name, int age) {
        this.nickName = name;
        this.age = age;
    }
}
