package hu.sonrisa.emplyeereport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmplyeereportApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmplyeereportApplication.class, args);
	}

}
