# Employee Report
Demo solution can be found on the “demo_solution” branch.

### Source: [https://codingdojo.org/kata/Employee-Report/](https://codingdojo.org/kata/Employee-Report/)

### Technology
- Java 8

## Introduction
You’re building an employee management system of a local grocery store. The shop-owner wants to open the shop on Sunday and due to legal restrictions employees younger than 18 years are not allowed to work Sundays. The employee asks for a reporting feature so she can schedule work shifts. All employees are already stored somewhere and have the following properties:
-   name: string (the name of the employee)
-   age: number (the age in years of the employee)

```js
const employees = [
  { name: 'Max', age: 17 },
  { name: 'Sepp', age: 18 },
  { name: 'Nina', age: 15 },
  { name: 'Mike', age: 51 },
];
```

## Stories
Start with the first user-story and write at least one test for every requirement. Try not to look on future requirements upfront and follow the TDD-Cycle strictly.

#### 1. As shop owner I want to view a list of all employees, which are older than 18 years, so that I know who is allowed to work on Sundays.
Implement GetEmployeesOlderThanEightTeen() method and the corresponding test.

#### 2. As shop owner I want the list of employees to be sorted by their name, so I can find employees easier.
Implement GetEmployeesByNameAscending() method and the corresponding test.

#### 3. As shop owner I want the list of employees to be capitalized, so I can read it better.
Implement GetEmployeesWithUppercaseNames() method and the corresponding test.

#### 4. As shop owner I want to know what is the average age of the employees
Implement GetAverageAge() method and the corresponding test.

## Extras 
Feel free to add more TDD test and implementations